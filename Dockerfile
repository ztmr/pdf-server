FROM alpine:3.8

RUN apk --update add openjdk8-jre tar so:libnss3.so

RUN mkdir /fop
WORKDIR /fop

COPY target/apache-fop-server-*-bin.tar.gz /fop/

RUN tar xfvz /fop/apache-fop-server-1.7-bin.tar.gz \
 && ln -s /dev/stdout /fop/logs/apache-fop-server.log

EXPOSE 9999
CMD ["java", "-cp", "apache-fop-server.jar:lib/*", "org.zilverline.fop.FopServer"]
