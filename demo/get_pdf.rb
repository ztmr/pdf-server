#!/usr/bin/env ruby

require 'thread'
require 'httpclient'

FOPSRV_URL  = "http://172.17.0.5:9999/pdf"
WORKERS     = 8
JOBS        = 256

class FopServerClient
  def get_pdf(xml, xsl, pwd)
    body = {xsl: xsl, xml: xml, pwd: pwd}
    client = HTTPClient.new
    result = client.post FOPSRV_URL, body
    result.body
  end
end

c = FopServerClient.new
q = Queue.new

(1..JOBS).each { |i| q.push i }

tt1 = Time.now
workers = (1..WORKERS).map do
  Thread.new do
    cnt = 0
    tw1 = Time.now
    tid = Thread.current.object_id
    begin
      while i = q.pop(true)
        puts "<#{tid}>: process: #{i}"
        t1 = Time.now
        c.get_pdf(IO.read("hello.fo"), IO.read("identity.xsl"), "pwd#{i}")
        t2 = Time.now
        puts "<#{tid}>: done: #{i} (#{t2-t1}s)"
        cnt += 1
      end
    rescue ThreadError
    end
    tw2 = Time.now
    puts "<#{tid}>: stats: total reqs #{cnt}, avg #{(tw2-tw1)/cnt}s"
  end
end.map(&:join)
tt2 = Time.now

puts "TOTAL: #{JOBS} requests in #{tt2-tt1}s"

pdf = c.get_pdf(IO.read("hello.fo"), IO.read("identity.xsl"), "testPassWord")
File.write('hello-with-user-password.pdf', pdf)

